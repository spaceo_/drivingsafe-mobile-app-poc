//
//  Constants.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 31/10/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

import UIKit

let STORYBOARD_MAIN = UIStoryboard.init(name: "Main", bundle: nil)

let NF_LOCATION_AUTO_STATUS = Notification.Name.init(rawValue: "NF_LocationAutorizationChange")
let NF_MOTION_AUTO_STATUS = Notification.Name.init(rawValue: "NF_MotionAutorizationChange")

let kUserBluetooth = "kuserbluetooths"

let _userDefaults = UserDefaults.standard

extension UIApplication {
    /// EZSE: Run a block in background after app resigns activity
    public func runInBackground(_ closure: @escaping () -> Void, expirationHandler: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            let taskID: UIBackgroundTaskIdentifier
            if let expirationHandler = expirationHandler {
                taskID = self.beginBackgroundTask(expirationHandler: expirationHandler)
            } else {
                taskID = self.beginBackgroundTask(expirationHandler: { })
            }
            closure()
            self.endBackgroundTask(taskID)
        }
    }
}

func SHOW_ALERT(_msg: String, cancel: String? , ok: String? , handler: @escaping(String) -> Void) {
    
    let alert = UIAlertController(title: "", message: _msg, preferredStyle: .alert)
    if cancel != nil {
        let cancelAction = UIAlertAction.init(title: cancel, style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
            handler(cancel!)
        }
        alert.addAction(cancelAction)
    }
    if ok != nil {
        let cancelAction = UIAlertAction.init(title: cancel, style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            handler(ok!)
        }
        alert.addAction(cancelAction)
    }
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    appDelegate.window?.rootViewController?.present(alert, animated: false, completion: nil)
}

struct ST_DateTimeFormater {
    
    static var DateTime: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm a"
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter
    }()
    
    static var Time: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm a"
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter
    }()
    
}
