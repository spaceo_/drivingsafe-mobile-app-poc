//
//  lockScreenStatus.m
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 14/11/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

#import "lockScreenStatus.h"
#import "notify.h"

@implementation lockScreenStatus

+(void)statusBlock:(void (^)(BOOL _status))block
{
    int notify_token;
    notify_register_dispatch("com.apple.springboard.lockstate", &notify_token, dispatch_get_main_queue(), ^(int token) {
        uint64_t state = UINT64_MAX;
        notify_get_state(token, &state);
        NSLog(@"com.apple.springboard.lockstate = %llu", state);
        if(state == 0) {
            block(NO);
        } else {
            block(YES);
        }
    });
}

- (void)registerForSomeNotifications
{
    //
    // Display notifications
    //
    
    __weak lockScreenStatus *weakSelf = self;
    
    uint32_t result = notify_register_dispatch(kNotificationNameDidChangeDisplayStatus.UTF8String,
                                               &_notifyTokenForDidChangeDisplayStatus,
                                               dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0l),
                                               ^(int info) {
                                                   __strong lockScreenStatus *strongSelf = weakSelf;
                                                   
                                                   if (strongSelf)
                                                   {
                                                       uint64_t state;
                                                       notify_get_state(self->_notifyTokenForDidChangeDisplayStatus, &state);
                                                       
                                                       strongSelf.displayOn = (BOOL)state;
                                                   }
                                               });
    if (result != NOTIFY_STATUS_OK)
    {
        self.registeredForDarwinNotifications = NO;
        return;
    }
    
    self.registeredForDarwinNotifications = YES;
}

- (void)unregisterFromSomeNotifications
{
    //
    // Display notifications
    //
    
    uint32_t result = notify_cancel(_notifyTokenForDidChangeDisplayStatus);
    if (result == NOTIFY_STATUS_OK)
    {
        self.registeredForDarwinNotifications = NO;
    }
}
@end
