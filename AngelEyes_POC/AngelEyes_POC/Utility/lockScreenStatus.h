//
//  lockScreenStatus.h
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 14/11/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
#define kNotificationNameDidChangeDisplayStatus @"com.apple.iokit.hid.displayStatus"

@interface lockScreenStatus : NSObject
{
    int _notifyTokenForDidChangeDisplayStatus;
}
@property (nonatomic, assign, getter = isDisplayOn) BOOL displayOn;
@property (nonatomic, assign, getter = isRegisteredForDarwinNotifications) BOOL registeredForDarwinNotifications;

+(void)statusBlock:(void (^)(BOOL _status))block;

@end

NS_ASSUME_NONNULL_END
