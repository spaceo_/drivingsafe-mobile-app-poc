//
//  StringExtension.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 06/12/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

import Foundation

extension String {
    func stringByAppendingPathComponent(path: String) -> String {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
}
