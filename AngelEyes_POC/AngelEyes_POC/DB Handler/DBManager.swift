//
//  DBManager.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 06/12/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

import Foundation
import SQLite3

let TABLE_TRIP_HISTORY = "tblTripHistory"

class DBManager: NSObject {
    
    private let dbFilename: String = "AngelEye.sqlite"
    private var documentsDirectory: String? = nil
    
    //private let dbPointer: OpaquePointer?
    
    override init() {
        super.init()
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        self.documentsDirectory = paths.first
        self.copyDatabaseIntoDocumentsDirectory()
    }
    
    
    private func copyDatabaseIntoDocumentsDirectory() {
        guard let directory = self.documentsDirectory else {
            print("documentsDirectory path not found.....")
            return
        }
        let destinationPath = directory.stringByAppendingPathComponent(path: self.dbFilename)
        let _fileManager = FileManager.default
        let isFileExist = _fileManager.fileExists(atPath: destinationPath)
        
        if isFileExist == false {
            guard let db = self.open() else {
                print("Not able to create databse")
                return
            }
            sqlite3_close(db)
        } else {
            print("======================DATABASE FOUND==========")
        }// END OF IF ELSE
    }// function end
    
    private func open() ->OpaquePointer?{
        guard let directory = self.documentsDirectory else {
            print("documentsDirectory path not found while opening data base .....")
            return nil
        }
        var db: OpaquePointer? = nil
        let path = directory.stringByAppendingPathComponent(path: self.dbFilename)
        //print(path)
        if sqlite3_open(path, &db) == SQLITE_OK {
            // 2
            return db
        } else {
            print("Not able to open database")
            defer {
                if db != nil {
                    sqlite3_close(db)
                }
            }
        }
        return nil
    }// endo of open function
    
    func getTripHistory() -> [M_Trip]? {
        
        guard let dbPointer = self.open() else { return nil }
        var statement: OpaquePointer? = nil
        
        let sqlQuery = "SELECT * FROM \(TABLE_TRIP_HISTORY) ORDER BY id DESC"
        var arrData = [M_Trip]()
        
        guard sqlite3_prepare_v2(dbPointer, sqlQuery, -1, &statement, nil) == SQLITE_OK else {
            print(String(cString: sqlite3_errmsg(dbPointer)))
            return nil
        }
        defer {
            sqlite3_finalize(statement)
            sqlite3_close(dbPointer)
        }
        
        while(sqlite3_step(statement) == SQLITE_ROW){
            
            let data = M_Trip()
            
            data.id = sqlite3_column_int(statement, 0)
            data.startTime = String(cString: sqlite3_column_text(statement, Em_TableTrip.startTime.GetColmSequence()))
            data.endTime = String(cString: sqlite3_column_text(statement, Em_TableTrip.endTime.GetColmSequence()))
            
            data.phoneMovedTime = String(cString: sqlite3_column_text(statement, Em_TableTrip.phoneMovedTime.GetColmSequence()))
            data.phoneLockTime = String(cString: sqlite3_column_text(statement, Em_TableTrip.phoneLockTime.GetColmSequence()))
            
            data.call_outgoingTime = String(cString: sqlite3_column_text(statement, Em_TableTrip.call_outgoingTime.GetColmSequence()))
            data.call_incomingTime = String(cString: sqlite3_column_text(statement, Em_TableTrip.call_incomingTime.GetColmSequence()))
            
            let _phoneMoved = sqlite3_column_int(statement, Em_TableTrip.phoneMoved.GetColmSequence())
            let _phoneLock = sqlite3_column_int(statement, Em_TableTrip.phoneLock.GetColmSequence())
            let _call_outgoing = sqlite3_column_int(statement, Em_TableTrip.call_outgoing.GetColmSequence())
            let _call_incoming = sqlite3_column_int(statement, Em_TableTrip.call_incoming.GetColmSequence())
            
            data.phoneMoved = Int16(_phoneMoved)
            data.phoneLock = Int16(_phoneLock)
            data.call_outgoing = Int16(_call_outgoing)
            data.call_incoming = Int16(_call_incoming)

            //adding values to list
            arrData.append(data)
        }
        return arrData
    }
    
    func CreateTable(tblName: String){
        
        guard let dbPointer = self.open() else { return }
        var statement: OpaquePointer? = nil
        
        guard sqlite3_prepare_v2(dbPointer, tblName, -1, &statement, nil) == SQLITE_OK else {
            print(String(cString: sqlite3_errmsg(dbPointer)))
            return
        }
        defer {
            sqlite3_finalize(statement)
            sqlite3_close(dbPointer)
        }
        
        guard sqlite3_step(statement) == SQLITE_DONE else {
            print("Not not able create table \(tblName)")
            return
        }
        print("Table created")
    }
    
    func InsertTrip(Obj: M_Trip){
        
        var param = Obj.Covert2Dict()
        
        var _row = "("
        var _value = "("
        
        for i in param {
            _row.append(i.key)
            _value.append("\'\(i.value)\'")
            
            _row.append(",")
            _value.append(",")
        }
        
        _row.removeLast()
        _value.removeLast()
        _row.append(")")
        _value.append(")")
        
        let insertSql = "INSERT INTO \(TABLE_TRIP_HISTORY) \(_row) VALUES \(_value);"
    
        guard let dbPointer = self.open() else { return }
        
        var statement: OpaquePointer? = nil
        
        guard sqlite3_prepare_v2(dbPointer, insertSql, -1, &statement, nil) == SQLITE_OK else {
            print(String(cString: sqlite3_errmsg(dbPointer)))
            return
        }
        defer {
            sqlite3_finalize(statement)
            sqlite3_close(dbPointer)
        }
        
        //executing the query to insert values
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(statement))
            print("failure inserting hero: \(errmsg)")
            return
        }
        print("Data inserted")
        
    }
    
    // Delete
    
    func DeleteAllTrip() {
        
        guard let dbPointer = self.open() else { return }
        var statement: OpaquePointer? = nil
        
        let sqlQuery = "DELETE FROM \(TABLE_TRIP_HISTORY)"
        
        guard sqlite3_prepare_v2(dbPointer, sqlQuery, -1, &statement, nil) == SQLITE_OK else {
            print(String(cString: sqlite3_errmsg(dbPointer)))
            return
        }
        defer {
            sqlite3_finalize(statement)
            sqlite3_close(dbPointer)
        }
    }
    
    
}
