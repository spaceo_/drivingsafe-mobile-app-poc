//
//  En_Trip+CoreDataClass.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 04/12/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//
//

import Foundation
import CoreData

@objc(En_Trip)
public class En_Trip: NSManagedObject {

    
    @nonobjc public class func getInstanceToInsertData() -> En_Trip? {
        return En_Trip(context: CoreDataHandler.shared.managedContext)
    }
    
    @nonobjc public class func saveData() {
        
        
        let entity =
            NSEntityDescription.entity(forEntityName: "En_Trip",
                                       in: CoreDataHandler.shared.managedContext)!
        
        let obj = NSManagedObject(entity: entity,
                                     insertInto: CoreDataHandler.shared.managedContext) as! En_Trip
        
        //let obj = En_Trip(context: CoreDataHandler.shared.managedContext)
        
        CoreDataHandler.shared.saveContext()
    }
    
    @nonobjc public class func fetchData() -> [En_Trip]? {
        
        do {
            let data = try CoreDataHandler.shared.managedContext.fetch(self.fetchRequest())
            guard let finalData = data as? [En_Trip] else {
                print("Error whitl fetching data")
                return nil
            }
            return finalData
        } catch let _error {
            print(_error.localizedDescription)
        }
        return nil
    }
}
