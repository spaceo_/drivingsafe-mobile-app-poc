//
//  En_Trip+CoreDataProperties.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 04/12/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//
//

import Foundation
import CoreData


extension En_Trip {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<En_Trip> {
        return NSFetchRequest<En_Trip>(entityName: "En_Trip")
    }
    @NSManaged public var startTime: String?
    @NSManaged public var endTime: String?
    @NSManaged public var phoneMoved: Bool
    @NSManaged public var phoneMovedTime: String?
    @NSManaged public var phoneLock: Bool
    @NSManaged public var phoneLockTime: String?
    @NSManaged public var call_outgoing: Bool
    @NSManaged public var call_outgoingTime: String?
    @NSManaged public var call_incoming: Bool
    @NSManaged public var call_incomingTime: String?
    @NSManaged public var sortDate: NSDate?
}
