//
//  M_Trip.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 06/12/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

import Foundation

class M_Trip : NSObject {
    
    var id: Int32?
    var startTime: String?
    var endTime: String?
    var phoneMoved: Int16 = 0
    var phoneMovedTime: String?
    var phoneLock: Int16 = 0
    var phoneLockTime: String?
    var call_outgoing: Int16 = 0
    var call_outgoingTime: String?
    var call_incoming: Int16 = 0
    var call_incomingTime: String?
    var sortDate: NSDate?
    
    func Covert2Dict() -> [String: Any] {
        var param = [String: Any]()
        param["startTime"] = self.startTime
        param["endTime"] = self.endTime
        
        param["phoneMoved"] = self.phoneMoved ?? 0
        param["phoneMovedTime"] = self.phoneMovedTime ?? ""
        
        param["phoneLock"] = self.phoneLock ?? 0
        param["phoneLockTime"] = self.phoneLockTime ?? ""
        
        param["call_outgoing"] = self.call_outgoing ?? 0
        param["call_outgoingTime"] = self.call_outgoingTime ?? ""
        
        param["call_incoming"] = self.call_incoming ?? 0
        param["call_incomingTime"] = self.call_incomingTime ?? ""
        
        return param
    }
}

enum Em_TableTrip {
    
    case id
    case startTime
    case endTime
    case phoneMoved
    case phoneMovedTime
    case phoneLock
    case phoneLockTime
    case call_outgoing
    case call_outgoingTime
    case call_incoming
    case call_incomingTime
    case sortDate
    
    static func CreateTable() -> String {
        return "create table \(TABLE_TRIP_HISTORY) (id integer primary key AUTOINCREMENT, startTime text, endTime text , phoneMoved INTEGER NULL, phoneMovedTime text NULL, phoneLock INTEGER NULL, phoneLockTime text NULL, call_outgoing INTEGER NULL, call_outgoingTime text NULL, call_incoming INTEGER NULL, call_incomingTime text NULL, sortDate text NULL)"
    }
    
    func GetColmSequence() -> Int32 {
        switch self {
        case .id: return 0
        case .startTime: return 1
        case .endTime: return 2
        case .phoneMoved: return 3
        case .phoneMovedTime: return 4
        case .phoneLock: return 5
        case .phoneLockTime: return 6
        case .call_outgoing: return 7
        case .call_outgoingTime: return 8
        case .call_incoming: return 9
        case .call_incomingTime: return 10
        case .sortDate: return 11
        }
    }
    
}
