//
//  SO_MotionManager.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 30/10/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

import UIKit
import CoreMotion

enum DriveSafeMode {
    case Driving,notDriving,inMovingVehicle,notAvailable
}

/* USER DEFAULT KEY FOR PHONE USES WHILE DRIVING..... */
let kDeviceLock = "kDeviceLockStatus"
let kCall_WithoutHandsFree = "kCallWithoutHandsFree"
let kCall_Dial = "kCallDial"

let KEY_FOR_DEVICE_USE = "deviceusedwhiledriving"

private let DrivignStatusLimit: Int = 60
let TRIP_DATA_SAVE_LIMIT = 1

class SO_MotionManager {
    
    /* shared varibale used as singalton ------------------------------*/
    static let shared = SO_MotionManager()
    private init() {}
    
    var backgroundStatusTimer: Timer?
    var backgroundModeHandler: (() ->Void)?
    
    /* core data entity object to store trip data */
    var tripEntityObj: M_Trip?
    
    /* ActivityManager object to get current activity ------------------*/
    var activityManager = CMMotionActivityManager.init()
    var lastActivity: CMMotionActivity = CMMotionActivity.init()
    
    
    //var CompletionActivityHandler: ((CMMotionActivity) ->Void)?
    /* isBluetoothConnected update when bluetooth device connection update--------------*/
    var isBluetoothConnected: Bool = false
    var isHeadPhoneConnected: Bool = false
    /* Clouser to get current activity-----------------------------*/
    var CompletionActivityHandler: ((DriveSafeMode) ->Void)?
    
    /* Clouser to get Speed . Used for Cllocation manager ---------------*/
    var SpeedHandler: ((Double) -> Void)?
    var userCurrentSpeed: Double = 0.0
    
    /* Incremental flag for driving status ------------------------*/
    private var currentDrivingLimit: Int = 0

    /* DrivingStatusTimer runs for 30 seconds to change status if user is already in driving --------------*/
    var DrivingStatusTimer: Timer?
    
    /* default driving mode when app launch --------------*/
    var DriveMode: DriveSafeMode = .notDriving
    
    
    /* userNotifiedForCallUses flag used to detect if user already notified for call uses  */
    var isPhoneUsedForCallWhileDriving: Bool = false
    
    var permisionHandlerForMostionManager: ((Bool) -> Void)?
    var permisionStatuOfMotion: Bool = false
    /* Handling Trip Data */
    func setTripInstance() {
        self.tripEntityObj = M_Trip()
    }
    
    /* StartMontionActivity function used to start MontionActivity -------- */
    func StartMontionActivity() {
        //timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        let _queue = OperationQueue.init()
        _queue.qualityOfService = .background
        
        //self.activityManager.queryActivityStarting(from: Date(), to: Date(), to: _queue) { (_activitys, _error) in
          //  print(_error?.localizedDescription)
        //}
        
        if self.permisionStatuOfMotion {
            self.permisionHandlerForMostionManager?(true)
        }else {
            DispatchQueue.global().asyncAfter(deadline: .now() + 3.0) {
                self.permisionHandlerForMostionManager?(false)
            }
        }
        if CMMotionActivityManager.isActivityAvailable() {
            
        }else {
            print(" Motion activity not available")
        }
        
        self.activityManager.startActivityUpdates(to: _queue) { (_activitys) in
            print("startActivityUpdates===========")
//            if let error = _error as? NSError , error.code == Int(CMErrorMotionActivityNotAuthorized.rawValue) {
//                self.permisionHandlerForMostionManager?(false)
//                self.CompletionActivityHandler?(self.DriveMode)
//                return
//            }
//            self.permisionHandlerForMostionManager?(true)
            self.permisionStatuOfMotion = true
            self.permisionHandlerForMostionManager?(true)
            guard let activity =  _activitys else {
                self.CompletionActivityHandler?(self.DriveMode)
                return
            }
            self.lastActivity = activity
            //print(self.lastActivity)
            
            guard activity.confidence.rawValue >= 1 else {
                self.CompletionActivityHandler?(self.DriveMode)
                return
            }
            
            switch self.isBluetoothConnected {
            case true :
                if activity.automotive && self.userCurrentSpeed > 10 {
                    self.DrivingStatusTimer?.invalidate()
                    self.DrivingStatusTimer = nil
                    self.currentDrivingLimit = 0
                    self.DriveMode = .Driving
                    self.CompletionActivityHandler?(self.DriveMode)
                }else if self.DriveMode == .Driving || self.DriveMode == .inMovingVehicle {
                    if self.DrivingStatusTimer == nil  {
                        DispatchQueue.main.async {
                            self.DrivingStatusTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateDrivingStatus), userInfo: nil, repeats: true)
                        }
                    }
                    self.DriveMode = .Driving
                    self.CompletionActivityHandler?(self.DriveMode)
                }else  {
                    self.DriveMode = .notDriving
                    self.CompletionActivityHandler?(self.DriveMode)
                }
                break
            case false:
                if activity.automotive && self.userCurrentSpeed > 10 {
                    self.DrivingStatusTimer?.invalidate()
                    self.DrivingStatusTimer = nil
                    self.currentDrivingLimit = 0
                    self.DriveMode = .inMovingVehicle
                    self.CompletionActivityHandler?(self.DriveMode)
                }else if self.DriveMode == .Driving || self.DriveMode == .inMovingVehicle  {
                    if self.DrivingStatusTimer == nil  {
                        DispatchQueue.main.async {
                            self.DrivingStatusTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateDrivingStatus), userInfo: nil, repeats: true)
                        }
                    }
                    self.DriveMode = .inMovingVehicle
                    self.CompletionActivityHandler?(self.DriveMode)
                }else {
                    self.DriveMode = .notDriving
                    self.CompletionActivityHandler?(self.DriveMode)
                }
                print("bluetooth off============================")
                break
            }// end of switch
        }// end of acitivity handler
    }// end of function
    
    
    @objc func updateDrivingStatus() {
        print("Timer for  updateDrivingStatus \(currentDrivingLimit)")
        self.currentDrivingLimit += 1
        if self.currentDrivingLimit > DrivignStatusLimit {
            self.DriveMode = .notDriving
            self.DrivingStatusTimer?.invalidate()
            self.DrivingStatusTimer = nil
            currentDrivingLimit = 0
            self.CompletionActivityHandler?(self.DriveMode)
        }
    }
    

    func StartBackgroundTimer()  {
        DispatchQueue.global(qos: .background).async {
            self.backgroundStatusTimer  = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.updateBackgroundStatus), userInfo: nil, repeats: true)
            let runLoop = RunLoop.current
            runLoop.add(self.backgroundStatusTimer!, forMode: .default)
            runLoop.run()
        }
        
    }
    func StopBackgroundTimer()  {
        self.backgroundStatusTimer?.invalidate()
        self.backgroundStatusTimer = nil
    }
    
    @objc func updateBackgroundStatus() {
       print("timer ==========")
    }
    
    //MARK:- USER DEVICE MOTION DETECTION
    
    let motionManager = CMMotionManager()
    var motionDataAfterDriving: [Double] = [Double]()
    var arrMotionFlag = [Bool]()
    
    
    func UserDeviceMostionDetection()  {
        
        
        self.arrMotionFlag = [false,false,false]
        motionManager.deviceMotionUpdateInterval = 0.5
        let limit: Double = 30
        let rolllimit: Double = 10
        let yawlimit: Double = 10
        
        motionManager.startDeviceMotionUpdates(to: .main) { (_motion, error) in
            guard error == nil , let motion = _motion else {
                let _error = error! as NSError
                print(error?.localizedDescription ?? "")
                print(error.debugDescription)
                print(_error.code)
                print(CMErrorMotionActivityNotAuthorized.rawValue)
//                if _error.code == Int(CMErrorMotionActivityNotAuthorized.rawValue) {
//                    NotificationCenter.default.post(name: NF_MOTION_AUTO_STATUS, object: CMErrorMotionActivityNotAuthorized)
//                }
                return
            }
            
            let pitch = round(motion.attitude.pitch * 180 / Double.pi)
            let roll = round(motion.attitude.roll * 180 / Double.pi)
            let yaw = round(motion.attitude.yaw * 180 / Double.pi)
    
            guard self.motionDataAfterDriving.count > 0 else {
                if self.DriveMode == .Driving {
                    self.motionDataAfterDriving = [pitch , roll ,yaw ]
                    self.arrMotionFlag = [false,false,false]
                    print(self.motionDataAfterDriving)
                }
                return
            }
            
            let storedPitch = self.motionDataAfterDriving[0]
            let storedRoll = self.motionDataAfterDriving[1]
            let storedYaw = self.motionDataAfterDriving[2]
            
            let _currentPitchDifference = (storedPitch) - (pitch)
            let _currentRollDifference = (storedRoll) - (roll)
            let _currentYawDifference = (storedYaw) - (yaw)
            
            let _finalPitchDifference = abs(_currentPitchDifference)
            let _finalRollDifference = abs(_currentRollDifference)
            let _finalYawDifference = abs(_currentYawDifference)
            
            if _finalPitchDifference > limit {
                self.arrMotionFlag[0] = true
            }
            
            if _finalRollDifference > rolllimit {
                self.arrMotionFlag[1] = true
            }
            
            if _finalYawDifference > yawlimit {
                self.arrMotionFlag[2] = true
            }
            
            let arr = self.arrMotionFlag.filter({ $0 == true })
            if arr.count >= 1 {
                if var arrLock = UserDefaults.standard.array(forKey: KEY_FOR_DEVICE_USE) as? [Bool] {
                    arrLock.append(true)
                    UserDefaults.standard.set(arrLock, forKey: KEY_FOR_DEVICE_USE)
                    UserDefaults.standard.synchronize()
                }
                self.motionDataAfterDriving = [pitch , roll ,yaw ]
                self.arrMotionFlag = [false,false,false]
                //self.motionManager.stopDeviceMotionUpdates()
                print(_finalPitchDifference)
                print(_finalRollDifference)
                print(_finalYawDifference)
                if let obj = SO_MotionManager.shared.tripEntityObj {
                    let _Time = ST_DateTimeFormater.Time.string(from: Date())
                    obj.phoneMovedTime = _Time
                    obj.phoneMoved = 1
                    //CoreDataHandler.shared.saveContext()
                }
            }
        }//

    }
    
    func ShowAlertForDeviceUses() {
        
        UserDefaults.standard.set(false, forKey: KEY_FOR_DEVICE_USE)
        UserDefaults.standard.synchronize()
        
        let alert = UIAlertController(title: "", message: "You used your phone while driving.", preferredStyle: .alert)
        let cancelAction = UIAlertAction.init(title: "Ok", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController?.present(alert, animated: false, completion: nil)
        }
    }
}
