//
//  AppDelegate.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 30/10/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

import UIKit
import CoreLocation
import CallKit
import AVFoundation
import Contacts

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var bgTask = UIBackgroundTaskIdentifier.invalid
    
    var callObserver: CXCallObserver!
    var store = CNContactStore()

    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager.init()
        manager.allowsBackgroundLocationUpdates = true
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        manager.pausesLocationUpdatesAutomatically = false
        manager.requestAlwaysAuthorization()
        return manager
    }()
    
    static var sharedDelegate: AppDelegate = {
        return UIApplication.shared.delegate as! AppDelegate
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let obj = DBManager.init()
        obj.CreateTable(tblName: Em_TableTrip.CreateTable())
        
        callObserver = CXCallObserver()
        callObserver.setDelegate(self, queue: nil)
        
        lockScreenStatus.statusBlock { (_status) in
            print(_status)
            let _currentMode = SO_MotionManager.shared.DriveMode
            if _status == false && _currentMode == .Driving {
                if var arrLock = UserDefaults.standard.array(forKey: kDeviceLock) as? [Bool] {
                    arrLock.append(true)
                    UserDefaults.standard.set(arrLock, forKey: kDeviceLock)
                    UserDefaults.standard.synchronize()
                }
                if let obj = SO_MotionManager.shared.tripEntityObj {
                    let _Time = ST_DateTimeFormater.Time.string(from: Date())
                    obj.phoneLockTime = _Time
                    obj.phoneLock = 1
                    //CoreDataHandler.shared.saveContext()
                }
            }else if _status == false && _currentMode == .inMovingVehicle {
                if let obj = SO_MotionManager.shared.tripEntityObj {
                    let _Time = ST_DateTimeFormater.Time.string(from: Date())
                    obj.phoneLockTime = _Time
                    obj.phoneLock = 1
                    //CoreDataHandler.shared.saveContext()
                }
            }
        }
        
        self.locationManager.startUpdatingLocation()
        
        //let device = UIDevice.current
        //device.isProximityMonitoringEnabled = true
        //NotificationCenter.default.addObserver(self, selector: #selector(sensorStateMonitor(_:)), name: UIDevice.proximityStateDidChangeNotification, object: device)
        
       
        
//        let trip = M_Trip()
//        trip.startTime = "\(Date())"
//        trip.endTime = "\(Date())"
//        trip.phoneLockTime = "\(Date())"
//        trip.phoneLock = 1
//
//        obj.InsertTrip(Obj: trip)
//
//        obj.getTripHistory()?.forEach({print($0.id)})
        
        print(_userDefaults.value(forKey: "APP_Terminate_TIME"))
        print(_userDefaults.value(forKey: "APP_BACKGROUND_TIME"))
        
       
        return true
    }

    @objc func sensorStateMonitor(_ sender: Notification) {
        
        guard let device = sender.object as? UIDevice else {
            return
        }
        if device.proximityState == true {
            print("Device is close to user")
        }else {
            print("Device is not closer to user")
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
         //self.locationManager.startUpdatingLocation()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        self.locationManager.startUpdatingLocation()
        bgTask = application.beginBackgroundTask(withName: "MyTask") {
            print("endBackgroundTask:bgTask")
            print("Time remaing... \(application.backgroundTimeRemaining)")
            application.endBackgroundTask(self.bgTask)
            self.bgTask = .invalid
        }
        print("Time remaing... \(application.backgroundTimeRemaining)")
        _userDefaults.set(Date(), forKey: "APP_BACKGROUND_TIME")
        _userDefaults.synchronize()
        
//        DispatchQueue.global(qos: .default).async {
//            let t = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.testTimer), userInfo: nil, repeats: true)
//            RunLoop.current.add(t, forMode: .default)
//            RunLoop.current.run()
//            
//        }
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        application.endBackgroundTask(self.bgTask)
        self.bgTask = .invalid
        
    }

    @objc func testTimer() {
        print("timer============================")
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//        application.endBackgroundTask(self.bgTask)
//        self.bgTask = .invalid
//        NotificationCenter.default.post(name: NSNotification.Name.init("UPDATE_DRIVING_STATUS"), object: nil)
        //self.locationManager.stopUpdatingLocation()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        CoreDataHandler.shared.saveContext()
        _userDefaults.set(Date(), forKey: "APP_Terminate_TIME")
        _userDefaults.synchronize()
    }

    func checkAccessStatus(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        
        let authorizationStatus = CNContactStore.authorizationStatus(for: .contacts)
        
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
        case .denied, .notDetermined:
            self.store.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }
                else {
                    print("access denied")
                    completionHandler(false)
                }
            })
        default:
            completionHandler(false)
        }
    }

}

extension AppDelegate: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        
        //print("=============User Location====== \(location.coordinate.latitude) \(location.coordinate.longitude) \(Date())")

        let _speed = location.speed * 3.6
        if _speed > 0 {
            SO_MotionManager.shared.userCurrentSpeed = ceil(_speed)
        }
        //print(_speed)
        //SO_MotionManager.shared.userCurrentSpeed = abs(15)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription )
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        NotificationCenter.default.post(name: NF_LOCATION_AUTO_STATUS, object: status)
        
//        if CLLocationManager.locationServicesEnabled() {
//            switch CLLocationManager.authorizationStatus() {
//            case .notDetermined, .restricted, .denied:
//                print("No access")
//            case .authorizedAlways, .authorizedWhenInUse:
//                print("Access")
//            }
//        } else {
//            print("Location services are not enabled")
//        }
    }
}


extension AppDelegate: CXCallObserverDelegate {
    
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        
        if call.isOutgoing == true {
            if let obj = SO_MotionManager.shared.tripEntityObj {
                let _Time = ST_DateTimeFormater.Time.string(from: Date())
                obj.call_outgoingTime = _Time
                obj.call_outgoing = 1
                //CoreDataHandler.shared.saveContext()
            }
            if SO_MotionManager.shared.DriveMode == .Driving {
                if var arrLock = UserDefaults.standard.array(forKey: kCall_Dial) as? [Bool] {
                    arrLock.append(true)
                    UserDefaults.standard.set(arrLock, forKey: kCall_Dial)
                    UserDefaults.standard.synchronize()
                }
            }
            
        } else if call.hasConnected == true && call.isOutgoing == false {
            
            if let obj = SO_MotionManager.shared.tripEntityObj {
                let _Time = ST_DateTimeFormater.Time.string(from: Date())
                obj.call_incomingTime = _Time
                obj.call_incoming = 1
                //CoreDataHandler.shared.saveContext()
            }
            let isHandsFreeConnected = SO_MotionManager.shared.isHeadPhoneConnected
            
            if isHandsFreeConnected == false && SO_MotionManager.shared.DriveMode == .Driving {
                if var arrLock = UserDefaults.standard.array(forKey: kCall_Dial) as? [Bool] {
                    arrLock.append(true)
                    UserDefaults.standard.set(arrLock, forKey: kCall_Dial)
                    UserDefaults.standard.synchronize()
                }
            } else  if isHandsFreeConnected == false && SO_MotionManager.shared.DriveMode == .inMovingVehicle {
                
            }
            
        }
    }
    
}
