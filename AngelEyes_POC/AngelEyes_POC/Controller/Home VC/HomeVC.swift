//
//  HomeVC.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 31/10/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

import UIKit
import CoreMotion
import AVFoundation
import CoreLocation

let APP_VERSION = "VERSION 1.0.0.10"
let PERMISSION_WARNING_IMAGE = UIImage.init(named: "warning")

class HomeVC: UIViewController {
    
    @IBOutlet weak var lblModeStatus: UILabel!
    @IBOutlet weak var lblActivityType: UILabel!
    @IBOutlet weak var lblAppVersion: UILabel!
    
    @IBOutlet weak var lblBluetoothConnecion: UILabel!
    @IBOutlet weak var viewBluetoothConnecion: UIView!
    
    @IBOutlet weak var viewDrivingMode: UIView!
    @IBOutlet weak var lblPhoneUsesCount: UILabel!
    
    @IBOutlet weak var viewLocationWarnings: UIView!
    @IBOutlet weak var viewMotionWarnings: UIView!
    
    @IBOutlet weak var viewLocationWarningsHieghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewMotionWarningsHieghtConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imgWarning: UIImageView!
    @IBOutlet weak var lblWarning: UILabel!
    
    var count = 0
    var strWarningMsgLocation = ""
    var strWarningMsgMotion = ""
    
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblAppVersion.text = APP_VERSION
        
        self.title = "Drive Safe"
        //self.view.backgroundColor = .green
        
        self.lblModeStatus.text = "Not Driving"
        self.viewDrivingMode.backgroundColor = UIColor.green
        self.lblBluetoothConnecion.text = "Not Connected"
        
        self.resetCounter()
        UserDefaults.standard.set([Bool](), forKey: KEY_FOR_DEVICE_USE)
        UserDefaults.standard.set([Bool](), forKey: kDeviceLock)
        UserDefaults.standard.set([Bool](), forKey: kCall_Dial)
        //UserDefaults.standard.set([Bool], forKey: kCall_WithoutHandsFree)
        UserDefaults.standard.synchronize()

        viewLocationWarningsHieghtConstraint.constant = 0
        viewMotionWarningsHieghtConstraint.constant = 0
        
        /* AVAudioSession is used to check connected bluetooth device */
        self.setupNotifications()
        let routeDescription = AVAudioSession.sharedInstance().currentRoute
        debugPrint("Current Routes : \(routeDescription)")
        for output in routeDescription.outputs {
            print("output ====================")
            self.navigationController?.navigationBar.barTintColor = .white
            
            if output.portType == AVAudioSession.Port.bluetoothA2DP ||  output.portType == AVAudioSession.Port.bluetoothHFP  || output.portType == AVAudioSession.Port.bluetoothLE {
                self.lblBluetoothConnecion.text = "Connected: \(output.portName)"
                //SO_MotionManager.shared.isBluetoothConnected = true
                DispatchQueue.main.async {
                    self.checkBluetoothCriterea(_devceName: output.portName)
                }
            }
            if output.portType == .headphones {
                print("headphones")
                SO_MotionManager.shared.isHeadPhoneConnected = true
            }
        }
        
        SO_MotionManager.shared.permisionHandlerForMostionManager = { (permision) in
            DispatchQueue.main.async {
                if permision == false {
                    self.viewMotionWarningsHieghtConstraint.constant = 20
                }else {
                    self.viewMotionWarningsHieghtConstraint.constant = 0
                }
            }
        }
        self.SetupActivityCallBack()
        SO_MotionManager.shared.StartMontionActivity()
        SO_MotionManager.shared.UserDeviceMostionDetection()
        
    }//
    
  
    
    deinit {
        print("HomeVC ====== deinit  ")
        NotificationCenter.default.removeObserver(self, name: AVAudioSession.routeChangeNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: NF_LOCATION_AUTO_STATUS, object: nil)
        NotificationCenter.default.removeObserver(self, name: NF_MOTION_AUTO_STATUS, object: nil)
    }
    
    func checkBluetoothCriterea(_devceName: String)  {
        if var arrBle = _userDefaults.array(forKey: kUserBluetooth) as? [String] {
            if arrBle.contains(_devceName) {
                SO_MotionManager.shared.isBluetoothConnected = true
                print("Bluetooth already exists============================")
            } else {
                guard arrBle.count < 2 else {
                    SO_MotionManager.shared.isBluetoothConnected = false
                    return
                }
                arrBle.append(_devceName)
                self.setUserForDriving(_names: arrBle)
            }
        } else {
            self.setUserForDriving(_names: [_devceName])
        }// end of else
    }
    
    func setUserForDriving(_names: [String]) {
        SHOW_ALERT(_msg: "Connected: \(_names.last ?? "") is your car bluetooth?", cancel: "No", ok: "Yes", handler: { (_str) in
            if _str == "Yes" {
                SO_MotionManager.shared.isBluetoothConnected = true
                _userDefaults.set(_names, forKey: kUserBluetooth)
                _userDefaults.synchronize()
            }else {
                SO_MotionManager.shared.isBluetoothConnected = false
            }
        })
    }
    
    func  resetCounter()  {
        self.lblPhoneUsesCount.text = "\(0)"
        self.count = 0
    }
    
    @objc func SetupWaringViewLocation(_ sender: Notification) {
        
        if let status = sender.object as? CLAuthorizationStatus {
            switch status {
            case .notDetermined, .restricted, .denied:
                viewLocationWarningsHieghtConstraint.constant = 20
            case .authorizedAlways, .authorizedWhenInUse:
                viewLocationWarningsHieghtConstraint.constant = 0
            }
        }
        
    }
    @objc func SetupWaringViewMotion(_ sender: Notification) {
        
//        if (sender.object as? CMError) != nil {
//            print(" motion Error ")
//            self.strWarningMsgMotion = "Warning: Motion serivce disabled."
//            var msg = self.strWarningMsgMotion
//            if self.strWarningMsgLocation.count > 0 {
//                msg = self.strWarningMsgMotion + "\n" + self.strWarningMsgLocation
//            }
//            DispatchQueue.main.async {
//                self.imgWarning.image = PERMISSION_WARNING_IMAGE
//                self.lblWarning.text = msg
//            }
//        }
    }
    //MARK:- CHECKING BLUETOOTH CONNECTIVITY
    
    func setupNotifications() {
        
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleRouteChange),
                                       name: AVAudioSession.routeChangeNotification,
                                       object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SetupWaringViewLocation(_:)), name: NF_LOCATION_AUTO_STATUS, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SetupWaringViewMotion(_:)), name: NF_MOTION_AUTO_STATUS, object: nil)
    }
    
    @objc func handleRouteChange(notification: Notification) {
        print("handleRouteChange(notification ====================")
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.barTintColor = .white
            guard let userInfo = notification.userInfo,
                let reasonValue = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt,
                let reason = AVAudioSession.RouteChangeReason(rawValue:reasonValue) else {
                    return
            }
            switch reason {
            case .newDeviceAvailable:
                let session = AVAudioSession.sharedInstance()
                for output in session.currentRoute.outputs {
                    if output.portType == AVAudioSession.Port.bluetoothA2DP ||  output.portType == AVAudioSession.Port.bluetoothHFP || output.portType == AVAudioSession.Port.bluetoothLE {
                        self.lblBluetoothConnecion.text = "Connected: \(output.portName)"
                        DispatchQueue.main.async {
                            self.checkBluetoothCriterea(_devceName: output.portName)
                        }
                        break
                    }
                    if output.portType == .headphones {
                        SO_MotionManager.shared.isHeadPhoneConnected = true
                    }
                }
            case .oldDeviceUnavailable:
                if let previousRoute =
                    userInfo[AVAudioSessionRouteChangePreviousRouteKey] as? AVAudioSessionRouteDescription {
                    for output in previousRoute.outputs {
                        if output.portType == AVAudioSession.Port.bluetoothA2DP ||  output.portType == AVAudioSession.Port.bluetoothHFP {
                            self.lblBluetoothConnecion.text = "Not Connected: \(output.portName)"
                            SO_MotionManager.shared.isBluetoothConnected = false
                            break
                        }
                        if output.portType == .headphones {
                            SO_MotionManager.shared.isHeadPhoneConnected = false
                        }
                    }
                }
            default: ()
            }
        }
    }// END OF BLUETOOTH CONNECTION NOTIFICATION HANDLER
    
    //MARK:- CALL BACK FROM ACTIVITY MANAGER
    func SetupActivityCallBack() -> Void {
        
        SO_MotionManager.shared.CompletionActivityHandler = { activity in
            //print("============CompletionActivityHandler=============")
            DispatchQueue.main.async {
                
                switch activity {
                case .Driving:
                    self.viewDrivingMode.backgroundColor = UIColor.red
                    self.lblModeStatus.text = "Driving"
                    self.resetCounter()
                    if let obj = SO_MotionManager.shared.tripEntityObj {
                        if obj.startTime == nil {
                            let _startTime = ST_DateTimeFormater.DateTime.string(from: Date())
                            obj.startTime = _startTime
                            //CoreDataHandler.shared.saveContext()
                        }
                    }else {
                        SO_MotionManager.shared.setTripInstance()
                    }
                    
                    break
                case .notDriving:
                    /* setting view ui */
                    self.viewDrivingMode.backgroundColor = UIColor.green
                    self.lblModeStatus.text = "Not Driving"
                    /* check for uses */
                    self.promteAlertForDeviceUse()
                    
                    /* save trip data */
                    if let obj = SO_MotionManager.shared.tripEntityObj , obj.endTime == nil {
                        let _endTime = ST_DateTimeFormater.DateTime.string(from: Date())
                        obj.endTime = _endTime
                        //CoreDataHandler.shared.saveContext()
                        if let _stDate = ST_DateTimeFormater.DateTime.date(from: obj.startTime!) , let _edDate = ST_DateTimeFormater.DateTime.date(from: _endTime) {
                            let difference = _edDate.seconds(from: _stDate)
                            if abs(difference) > TRIP_DATA_SAVE_LIMIT {
                                DBManager().InsertTrip(Obj: obj)
                            }// end of if
                        }// enf of if
                    }// end of if let
                    if SO_MotionManager.shared.tripEntityObj != nil {
                        SO_MotionManager.shared.tripEntityObj = nil
                    }
                    break
                case .inMovingVehicle:
                    self.viewDrivingMode.backgroundColor = UIColor.red
                    self.lblModeStatus.text = "In moving vehicle"
                    self.resetCounter()
                    
                    if let obj = SO_MotionManager.shared.tripEntityObj {
                        if obj.startTime == nil {
                            let _startTime = ST_DateTimeFormater.DateTime.string(from: Date())
                            obj.startTime = _startTime
                            //CoreDataHandler.shared.saveContext()
                        }
                    }else {
                        SO_MotionManager.shared.setTripInstance()
                    }
                    break
                case .notAvailable:
                    self.lblModeStatus.text = "Device Not Supported Core Motion Activity."
                    break
                }// END OF SWITCH
//                let _state = SO_MotionManager.shared.lastActivity
//                if _state.automotive {
//                    self.lblActivityType.text = "Driving : automotive"
//                } else if _state.stationary {
//                    self.lblActivityType.text = "Not Driving : stationary"
//                }else if _state.cycling {
//                    self.lblActivityType.text = "Driving : cycling"
//                }else if _state.walking {
//                    self.lblActivityType.text = "Not Driving : walking"
//                }else if _state.running {
//                    self.lblActivityType.text = "Not Driving : running"
//                }else if _state.unknown {
//                    self.lblActivityType.text = "Not Driving : unknown"
//                }
                
            }//
        }// END OF HANDLER
    }
    
    //
    func promteAlertForDeviceUse() {
        
        if let usesFlag = UserDefaults.standard.array(forKey: KEY_FOR_DEVICE_USE) {
            count += usesFlag.count
        }
        if let _deviceLock = UserDefaults.standard.array(forKey: kDeviceLock) {
            count += _deviceLock.count
        }
        if let _callDialled = UserDefaults.standard.array(forKey: kCall_Dial) {
            count += _callDialled.count
        }
        let _callWithoutHF = UserDefaults.standard.bool(forKey: kCall_WithoutHandsFree)
        
        UserDefaults.standard.set([Bool](), forKey: KEY_FOR_DEVICE_USE)
        UserDefaults.standard.set([Bool](), forKey: kDeviceLock)
        UserDefaults.standard.set([Bool](), forKey: kCall_Dial)
        //UserDefaults.standard.set([Bool], forKey: kCall_WithoutHandsFree)
        UserDefaults.standard.synchronize()
        DispatchQueue.main.async {
            self.lblPhoneUsesCount.text = "\(self.count)"
        }
        
        SO_MotionManager.shared.motionDataAfterDriving.removeAll()
        
//        if usesFlag || _deviceLock || _callDialled || _callWithoutHF {
//
//            let alert = UIAlertController(title: "", message: "You used your phone while driving.", preferredStyle: .alert)
//            let cancelAction = UIAlertAction.init(title: "Ok", style: .cancel) { (action) in
//                alert.dismiss(animated: true, completion: nil)
//            }
//            alert.addAction(cancelAction)
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.window?.rootViewController?.present(alert, animated: false, completion: nil)
//
//            UserDefaults.standard.set(false, forKey: KEY_FOR_DEVICE_USE)
//            UserDefaults.standard.set(false, forKey: kDeviceLock)
//            UserDefaults.standard.set(false, forKey: kCall_Dial)
//            UserDefaults.standard.set(false, forKey: kCall_WithoutHandsFree)
//            UserDefaults.standard.synchronize()
//
//            SO_MotionManager.shared.motionDataAfterDriving.removeAll()
//            SO_MotionManager.shared.UserDeviceMostionDetection()
//        }
    }
    // checking driving status
    
    func ShowAlertForDeviceUses() {
        let alert = UIAlertController(title: "", message: "You used your phone while driving.", preferredStyle: .alert)
        let cancelAction = UIAlertAction.init(title: "Ok", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        DispatchQueue.main.async {
            UserDefaults.standard.set(false, forKey: KEY_FOR_DEVICE_USE)
            UserDefaults.standard.synchronize()
            SO_MotionManager.shared.UserDeviceMostionDetection()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController?.present(alert, animated: false, completion: nil)
        }
    }
  
}

