//
//  cell_TripHistory.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 05/12/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

import UIKit

class cell_TripHistory: UITableViewCell {

    @IBOutlet weak var lbl_TripNo: UILabel!
    
    @IBOutlet weak var lbl_TripStartTime: UILabel!
    @IBOutlet weak var lbl_TripEndTime: UILabel!
    
    @IBOutlet weak var lbl_LockStatus: UILabel!
    @IBOutlet weak var lbl_LockTime: UILabel!
    
    @IBOutlet weak var lbl_CallOutgingStatus: UILabel!
    @IBOutlet weak var lbl_CallOutgingTime: UILabel!
    
    @IBOutlet weak var lbl_CallIncommingStatus: UILabel!
    @IBOutlet weak var lbl_CallIncomingTime: UILabel!
    
    @IBOutlet weak var lbl_phoneMoveStatus: UILabel!
    @IBOutlet weak var lbl_phoneMoveTime: UILabel!
    
    var strTripNo: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        lbl_TripNo.text = ""
        lbl_TripStartTime.text = ""
        lbl_TripEndTime.text = ""
        
        lbl_LockStatus.text = ""
        lbl_LockTime.text = ""
        
        lbl_CallOutgingStatus.text = ""
        lbl_CallOutgingTime.text = ""
        
        lbl_CallIncommingStatus.text = ""
        lbl_CallIncomingTime.text = ""
        
        lbl_phoneMoveStatus.text = ""
        lbl_phoneMoveTime.text = ""
    }

    var tripObj : M_Trip! {
        didSet {
            
            lbl_TripNo.text = "Trip #\(tripObj.id ?? 0)"
            lbl_TripStartTime.text = tripObj.startTime ?? ""
            lbl_TripEndTime.text = tripObj.endTime ?? ""
            
            lbl_LockStatus.text = "Screen Lock/Unlock : No"
            lbl_LockTime.text = ""
            if tripObj.phoneLock == 1 {
                lbl_LockStatus.text = "Screen Lock/Unlock :Yes"
                lbl_LockTime.text = tripObj.phoneLockTime ?? ""
            }
            
            lbl_CallOutgingStatus.text = "Outgoing Call : No"
            lbl_CallOutgingTime.text = ""
            if tripObj.call_outgoing == 1 {
                lbl_CallOutgingStatus.text = "Outgoing Call : Yes"
                lbl_CallOutgingTime.text = tripObj.call_outgoingTime ?? ""
            }
            
            lbl_CallIncommingStatus.text = "Incoming Call : No"
            lbl_CallIncomingTime.text = ""
            if tripObj.call_incoming == 1 {
                lbl_CallIncommingStatus.text = "Incoming Call : Yes"
                lbl_CallIncomingTime.text = tripObj.call_incomingTime ?? ""
            }
            
            lbl_phoneMoveStatus.text = "Phone Movement : No"
            lbl_phoneMoveTime.text = ""
            if tripObj.phoneMoved == 1 {
                lbl_phoneMoveStatus.text = "Phone Movement : Yes"
                lbl_phoneMoveTime.text = tripObj.phoneMovedTime ?? ""
            }
        }
    }

}
