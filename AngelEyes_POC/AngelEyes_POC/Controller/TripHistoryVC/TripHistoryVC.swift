//
//  TripHistoryVC.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 05/12/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

import UIKit

class TripHistoryVC: UITableViewController {

    private let cellIdenfire = "cellIdenfire"
    
    var arrTripData = [M_Trip]()
    
    lazy var emptyDataLbl: UILabel = {
        let lbl = UILabel.init()
        let width = UIScreen.main.bounds.width - 20
        lbl.frame = CGRect.init(x: 0.0, y: 100.0, width: width, height: 100)
        lbl.font = UIFont.systemFont(ofSize: 17)
        lbl.textColor = .black
        lbl.center = self.view.center
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.text = "No trip found."
        return lbl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        
        self.title = "Trip History"
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _arr = DBManager().getTripHistory() {
            self.arrTripData = _arr
            self.tableView.reloadData()
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        self.emptyDataLbl.removeFromSuperview()
        if arrTripData.count <= 0 {
            self.view.addSubview(self.emptyDataLbl)
        }
        return arrTripData.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdenfire , for: indexPath) as! cell_TripHistory
        //cell.strTripNo = "Trip #\(indexPath.row + 1)"
        cell.tripObj = arrTripData[indexPath.row]
        return cell
    }
    

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 195
    }

}
