//
//  SpeedVC.swift
//  AngelEyes_POC
//
//  Created by SOTSYS017 on 31/10/18.
//  Copyright © 2018 SOTSYS017. All rights reserved.
//

import UIKit
import CoreMotion

class SpeedVC: UIViewController {
    
    @IBOutlet weak var lblModeStatus: UILabel!
    
    let motionManager = CMMotionManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Device Angle"
        
        motionManager.deviceMotionUpdateInterval = 0.5
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        motionManager.startDeviceMotionUpdates(to: .main) { (_motion, error) in
            guard error == nil , let motion = _motion else {
                return
            }
            
            let pitch = round(motion.attitude.pitch * 180 / Double.pi)
            let roll = round(motion.attitude.roll * 180 / Double.pi)
            let yaw = round(motion.attitude.yaw * 180 / Double.pi)
            
            let _finalPitch = abs(pitch)
            let _finalRoll = abs(roll)
            let _finalyaw = abs(yaw)
            
            DispatchQueue.main.async{
                self.lblModeStatus.text = "Degree \(_finalPitch)" + "\n\(_finalRoll)" + "\n\(_finalyaw)"
            }
            
        }//
        
       
    }
}


